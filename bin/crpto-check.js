const program = require("commander");
const check = require("../commends/check")

program
.command('price')
.description("Check price of coins")
.option("--coin <type>",'Add specific coin types in CSN fromat', "BTC,ETH,XRP")
.option("--cur <currency>","Change the currency" ,'USD')
.action((cmd) => check.price(cmd));

program.parse(process.argv)